git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------
Comandos Basicos

git init
Inicia um repositório local.

git status
Mostra um panorama geral do seu projeto, se há modificações prontas para commitar(staged) ou se há alterações em arquivos ainda não rastreadas pelo git(untracked).

git add arquivo/diretório
Coloca todas as modificações  feitas no arquivo/diretório especifico para o estágio staged, isto é, pronto para commitar e indica ao git que esse arquivo/diretório deve ser rastrado.

git add --all = git add .
Adiciona todas as modificações realizadas no repositório para o estágio staged, isto é, pronto para commitar.
Porém, o uso do git add . deve ser feito com atenção pois não é recomendável agrupar mudanças no projeto que não estão diretamente relacionadas em um mesmo commit.
Exemplo de uso: Se você fizer alterações na página inicial do seu projeto e também na página de cadastro, o ideal é que essas alterações sejam colocadas em commits diferentes, logo, não é recomendável usar o “git add .”

git commit -m “Primeiro commit”
Realiza o commit de todos os arquivos modificados que estavam sendo rastreados pelo git, isto é todos os arquivos que estavam como “staged, com a seguinte mensagem “Primeiro commit” que descreve o que foi alterado no commit.
O ideal é que as mensagens de commit sejam pequenas, claras e objetivas. 

-------------------------------------------------------
Comando log

git log
Lista todos os commits e suas respectivas informações(data, id, email do autor, mensagem do commit).

git log arquivo
Lista todos os commits de um arquivo especifico do projeto.

git reflog
Lista todos os commits de uma forma mais resumida.(parte do id e a mensagem do commit)

-------------------------------------------------------
Comando Show 

git show
Mostra as alterações feitas no seu último commit.

git show <commit>
Para obter o id de um commit basta usar git reflog e copiar o número referente ao commit que você quer analisar.

-------------------------------------------------------

Comando Diff

git diff
Mostra a diferença entre o que você modificou no projeto(e que ainda não foi commitado) e o seu commit mais recente.

git diff <commit1> <commit2>
Faz a comparação entre dois commits.
-------------------------------------------------------
Comando Reset 

git reset --hard <commit>
Desfaz um commit especifico.
-------------------------------------------------------
Branches e seus comandos

git branch
Lista todas as branches locais

git branch -r 
Lista todas as branches remotas.

git branch -a
Lista todas as branches locais e remotas

git branch -d <branch_name> 
Remove uma branch local

git branch -D <branch_name> 
Força a remoção de um branch local. (Necessário quando queremos deletar um branch que ainda não foi feito o merge)

git branch -m <nome_novo>
Renomeia a branch com um novo nome.

git branch -m <nome_antigo> <nome_novo>
Similar ao comando anterior, renomeia a branch especificada em “nome_antigo” com um “nome_novo”. Não é necessário especificar caso você já esteja na branch cujo nome será alterado.
-------------------------------------------------------
Comando checkout

git checkout <branch_name> 
Muda o código do projeto para a branch selecionada. É necessário commitar as alterações antes de trocar de branches.

git checkout -b <branch_name> 
Cria uma nova branch e já faz a troca do versionamento para a nova branch.
-------------------------------------------------------
Comando MERGE 

git merge <branch_name> 
Faz a fusão de duas branches.

-------------------------------------------------------
Comandos para repositório remoto

git clone
Realiza a clonagem do repositório para a pasta atual. A origem do repositório pode vir de uma pasta local ou de uma URL para o código ser baixado. 

git pull
Atualiza o seu repositório local com a versão mais recente da origem da branch remota. Se for no servidor, a branch deve estar no servidor para que o git a compare com a versão local.

Se for no repositório local, deve ter sido clonada de outra pasta para a pasta destino. 

git push
Faz o envio das mudanças comitadas no seu repo local para a origem da branch rastreada.
-------------------------------------------------------
Comando remote

git remote -v
Faz a listagem dos servidores remotos que o repositório está usando associados com a URL 

git remote add origin <url>
Adiciona a URL do servidor associado ao repositório remoto para a variável origin

git remote <url> origin
Faz a troca da URL do servidor 'origin' para a nova URL informada. Caso exista mais de um
servidor remoto que o repositório esteja sendo rastreado, a palavra 'origin' pode ser substituída.

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLucm8g_ezqNq0dOgug6paAkH0AQSJPlIe

Vídeo sobre Git: 
https://www.youtube.com/watch?v=kB5e-gTAl_s
